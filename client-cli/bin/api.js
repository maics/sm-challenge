import fetch from 'node-fetch';
import { API_URL } from './config';

export const getIssues = async () => {
	const response = await fetch(`${API_URL}/issues`);
	const data = await response.json();
	console.log(data);
	return data;
};

export const postIssue = async (body) => {
	const response = await fetch(`${API_URL}/issues`, {
		method: 'post',
		body: JSON.stringify(body),
		headers: { 'Content-Type': 'application/json' },
	});

	const data = await response.json();

	console.log(data);
	return data;
};
