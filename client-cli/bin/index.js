#! /usr/bin/env node
console.log('Welcome to REST CLI');
const myArgs = process.argv.slice(2);

import { getIssues, postIssue } from './api';

switch (myArgs[0]) {
	case 'get':
		getIssues().then(() => console.log('done!'));
		break;
	case 'post':
		const param = { title: 'New Issue', description: 'description' };
		postIssue(param).then(() => console.log('done!'));
		break;
	default:
		break;
}
