import Issue from '../domain/Issue';

export class IssueRepository {
	issuesMockData = [
		new Issue('1', 'Bug: Unable to login', 'Description'),
		new Issue('2', 'Bug: Unable to logout', 'Description'),
		new Issue('3', 'Bug: Unhandled exception', 'Description'),
	];

	getIssues() {
		return this.issuesMockData;
	}

	getIssue(issueId) {
		return this.issuesMockData.find((item) => item.id === issueId);
	}

	updateIssue(issueId, title, description) {
		const updatedIssue = { id: issueId, title, description };

		this.issuesMockData = this.issuesMockData.map((issue) => {
			if (issue.id === issueId) {
				return { ...updatedIssue };
			}
			return issue;
		});

		return updatedIssue;
	}

	deleteIssue(issueId) {
		const issue = this.getIssue(issueId);
		this.issuesMockData = this.issuesMockData.filter(
			(issue) => issueId !== issue.id
		);
		return issue;
	}
}
