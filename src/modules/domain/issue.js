export default class Issue {
	constructor(id, title, description) {
		this.id = id;
		this.title = title;
		this.description = description;
	}
}
