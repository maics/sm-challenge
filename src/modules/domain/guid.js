import { uuid } from 'uuidv4';

export class Guid {
	constructor(value) {
		this.value = value || uuid();
	}
}
