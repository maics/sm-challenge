import { issueRepository } from '../modules/repos';
import Issue from '../modules/domain/Issue';
import { Guid } from '../modules/domain/guid';

export const create = async (req, res) => {
	const { title, description } = req.body;

	if (!title || !description) {
		return res.status(400).send({ error: 'Invalid request' });
	}

	const issue = new Issue(new Guid().value, title, description);

	res.send(issue);
};

export const getAll = async (req, res) => {
	const issues = issueRepository.getIssues();
	res.send(issues);
};

export const get = async (req, res) => {
	const { issueId } = req.params;

	const issue = issueRepository.getIssue(issueId);

	if (!issue) {
		return res.status(404).send({ error: 'Issue not found' });
	}

	res.send(issue);
};

export const update = async (req, res) => {
	const { issueId } = req.params;

	const { title, description } = req.body;

	const updatedIssue = issueRepository.updateIssue(issueId, title, description);

	res.send(updatedIssue);
};

export const remove = async (req, res) => {
	const { issueId } = req.params;

	const deletedIssue = issueRepository.deleteIssue(issueId);

	res.send(deletedIssue);
};
