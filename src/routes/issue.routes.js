import express from 'express';
import * as issues from '../controllers/issue.controller';

const router = express.Router();

router.post('/', issues.create);
router.get('/', issues.getAll);
router.get('/:issueId', issues.get);
router.put('/:issueId', issues.update);
router.delete('/:issueId', issues.remove);

export default router;
