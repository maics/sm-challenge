import express from 'express';
import issueRouter from './issue.routes';

const router = express.Router();

router.use('/issues', issueRouter);

export default router;
