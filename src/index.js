import express from 'express';
import apiRouter from './routes';
const { PORT = 3000 } = process.env;

const app = express();

app.use(express.json());

app.get('/', (_, res) => {
	res.json({ message: 'Welcome to Issues api.' });
});

app.use('/api/v1', apiRouter);

app.listen(PORT, () => {
	`Issues API is up and running on port ${PORT}`;
});
